## Context Understanding

I believe that atleast once, everyone here would have contributed to any one of the collaborative projects like, Wikipedia, Open Street Maps or similar projects in terms
of building technology, content publishing, documentation, etc. Now think about it, with all those data and information contributed by the commons, what will happen
if such hard work potential is used for **only for profit** business models flowing towards to only a few % of the model itself ? Will that progress such collaborative
projects or regress and exploit the commons contributions ?

### Example:

- OSM has huge collection of data points contributed by the community/commons/peers
- + Already is connecting with Wikipedia through Wikidata (Semantic Web)

- With "Startup Ecosystems" accelerated by national & international business systems, corporates, & governments is it justifiable to use such collection of data, by
  the VC funded startups, who have not contributed anything earlier in building the infrastructure itself ? 
- Who generates the data ?
- Who owns the data ?
- Who owns the collaborative projects ?
- Who uses the data ?
- How come "free loading" in "Sharing economy" is working ?
- Who enjoys the result of transformation : { DATA ==> Streaming Services } ???
- Who manages the mechanism of such transformation ? Do they enjoy the fruits ?
- Are these projects community built ? Are they transparent & accessible for participation ?

#### Think about it locally !

What will happen, if through Digital India, some startup, wants to harness all the wikidata, OSM feeds, host their powerful service in AWS, which is adapting a 
closed source technological business model, and provide a awesome UX service ? Say they are offering a transportation service (i took transport as example because
most of us can easily understand it) that are cheaper than the public transport, and provide a review feedback system that effectively regulates the service quality
and offers freebies along with massive PA, marketing, lobbying support from the government itself, so that public policies can be easily tilted in favour for thier
production/exploitation method.

## One perspective reveals:

- **New mode of production** : Collaborating Commons/Peers
- **Effective mode of Transformation** : Centrlized Streaming service technology
- **New mode of Ownership** : Service erectors
