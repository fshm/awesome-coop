
# To Organize & Manage with cooperative principles & strategies:

It is necessary that a cooperative - especially worker cooperative have to be managed by the 
constituent members called the cooperators. The very fundamental requirement for a worker
cooperative is that these cooperators be **knowledgeable** about the process, strategies, principles, philosophies underlying the cooperativei itself - translating into transparency, accountability, democracy, education as a complete and inclusive process.

No sole decision is taken as an ultimate authority does in sole proprietorship or in a private ownership of a platform and means of production. Thus, only keeping the process and other abstract ideas, stragies and stuff transparent and democratic, is of no value when the operators are not knowledgeable and empathetic. Thus the collective function and progress of the community depends upon educating the individuals to evolve them as cooperators.

The following stuff i share is what i consider to be helpful for individual and community as a whole to educate themselves, discuss about it, and if possible practice it so that ideas of the interest groups and the community can be transformed into a sustainable produce.

- Our understanding of the world & Systems are different from others.
- Its arrogant to expect, that people external to the community, must understand our principles, situations and practices.
- Do not hesitate to ideate, and dont get intimidated by the existing exhaustive solutions.
- If the solutions do have something interesting and entertaining, then it most likely can achieve a use value
- If you are not contend with an existing solution, then do make sure what improvement can be contributed or alternative can be developed to be unique or present as unique
- Do not start a ideation, discussion, proposal, implementation, etc... with a pessimistic view.
- Be realistic and critical.
- Do not afraid to make mistakes, it is a progressive step towards atleast learning. Otherwise, there will be nothing called **experience**.
- Do not take a task, just because it seems to be easier, even when it is really easier. Do approach the tasks which you find interesting can be confident with contribution.
- Be ready to learn concepts, and stuff from the peers (technical, architectural, philosophical). Understand the learning curve involved when learning it yourself, so that you can stay realistic, and see if you can shorten it by arranging a knowledge sharing meeting to learn stuff directly from the peers. This is part of cooperative principles pertaining to education.


## S.M.A.R.T :

I learned this strategy from a Transport and City planning/Implementation guide book. What does it says ?

- **S** ==> *SPECIFIC*
- **M** ==> *MEASURABLE*
- **A** ==> *AGREED UPON*
- **R** ==> *REALISTIC*
- **T** ==> *TIME BASED*

Every individual (cooperator) need to manage Information & Time effectively, so that they can be productive to themselves, which will turn into owning their work/labour/effort for which they like to contribute to. Every special interest group in the community, will have a work plan, road map, which is integrated from milestones, which can be seen as collection of tasks, which the contributors in the SIG will perform to reach a particular milestone. Thus the Time and Information are actually the Task each individual volunteer based on their expertise or interest/passion, which is converted into work/effort/labour.

> i.e, **TASK** = TIME + INFORMATION + EXPERTISE/INTEREST

> **LABOUR** <-- is derived from -- **TASKS**

> **PRODUCE** <-- is derived from -- **COLLECTIVE LABOUR** of a SIG

The produce can either be a tangible product or a service, but it shall be of **USE-VALUE** to the consumer so that they would realize the **necessity** of the product, and the community shall derive revenue from distributing and circulating the produce as good, with current exchange mechanisms, where the community actually has to interface with the hard reality of the systems.


## SWOT :

I have not personally tried SWOT for much. But it is one of the planning and management tool out there that equips the group in business. Make sure to atleast learn it. Try it collectively and individually.


## META SHARING :

In a cold blooded view, a cooperative is a group. And every group will have its own unpredictable group dynamics emerging with time and circumstances. A group will understand its dynamics when flowing along with it, in a **critical way**. An important factor about it during the intial periods of community emergence is meta sharing, where the members in a specific SIG or the commune as a whole, would **share** their **tabulated expertise and ignorance** with each other in a common platform. This is the real intiator of dynamics in the group. Once there is troughs of expertise and valleys of ignance, a relationship/connection will emerge between peers to share and learn stuff.

Thus Individually :

> **Tabulate : Known Knowns, Known Unknowns**

Then comeup with a manual or automated solution, which enables the community to form a new SIG, based on a market requirement from survey, or a new innovation that emerged from one of the cooperators mind. Then we will know whose expertise we need with what level of **strength** etc.. The decision will be easier to make without any personal biases as much as possible.


## COOERATORS STACK :

The following illustration is completely based upo my retrospective and introspective analysis, gained from studying, and research other coops, their priciples etc... **This might be even wrong.** But i find the necessity **not to impose hidden hierarchy of control** in a cooperative - which will leave a scan in every cooperators and devastating effects on the organization itself. Thus to provide a structure is necessity, while to limit the domination that derives from the strcutre is bit hard even to digest. Let's see how it goes... :)

Please feel free to edit it if want to... ( created with inkscape only ). The SVG file is in res directory.

![arch-struct-stack](res/architecture-structure-stack.png)

### parts :

**Organizational :** The bedrock of principles, philosophies, that actually united the cooperative in existence. Answers, **WHY ?** the coop is there in first place & **WHY ?** it is a coop. Discusses about the principles, constitution, governance, decision making process, deliberation, etc..

**Informatonal :** Overall transparency of the coop. It also represents the functinoal, network, management of projects and resources layers' information to be transparent, so that specificity can be answered and held accountable. Thus it spans both the verticals and the horizontals in the stack. Stripping it away will make the organization dysfunctional. 

**Functional :** What are the functions of the cooperative in terms of management and organizing itself as well as towards producing what the cooperative and its SIG's desires to. This eventually boils down to the functional roles taken up by the cooperators.

**Network :** This represents the network of the topology about the architecture explaining, how the functions or functional roles are assigned to the cooperators, how the functions are connected to each other's functions. This connections logically emerges into a network, which when transparently shared, enables the cooperators understand the dynamics of the group/coop in its current state. This usually decides not only the success rate, but also enables cooperators to share ideas in the realm of confusion, different view points, etc.... This is what we claim as **non turbulent stability**

**Resources :** Any cooperative or here any group would require basic infrastructure to form a minimum viable produce. In this case, it need not be just a financial supply, it can be economic supplies shared mutually that erects the infrastructure that enables the SIG's to function properly, focus on the problem, productive and actually deal with it without any clutter and confusion. Nowadays, with the advent of communication, many developers are comfortable with working remotely. But this need not be the case and often humans require physical meeting and rubbing shoulders play a significant rational and explorative bonding. Resources specific to project also comes under its management. Collective revenue generation, and specific SIG revenue generation has to be managed - transparently without affecting each others labour.

**Project :** Having an idea or an implementatble road way does not makes a project. We can have a prototype and even a stable working produce. However, unless it creates a usevalue to a consumer/client, with which the SIG or community can earn the fruits transparently makes what we called as project. Every project requires multitude of resources from key expertise, knowledge sharing, infrastructure, time, tasks, ideation, impression, delivery, quality, etc... This is where teams - SIG's play vital role.

**Outcomes :** When projects are managed by corresponding SIG's, each project will result in a produce, and it becomes the SIG's effort along with the cooperatives collective market knowledge, brain storming, survey, etc... to know the demand, supply, existing competition, channels to approach the client/consumer/public/authority or anybody to whom the produce will be valuable. It will be a huge study of alternative marketing strategies and delivery of produce where it is actually valuable. Failure of not launching the product for a number of reasons, does not deem the produce to be failure. It is just denoted with failure in one key aspect. Thus the project and minimum viable product will be preserved, if possible maintained which might suit future reqruiement. However further decisions are to be taken by the SIG that is responsible for it.


