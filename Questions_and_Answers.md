- What is Cooperativism ?
- What is Platform Cooperativism ?
- How practical are the principles ?
- Why not "Startup/VC Business" Model ?
- Why not "Sharing Economy" ?
- What is Business Model Generation ?
- What is Human Centric - Non-greedy Mode of Production ?
- Connections & Relationship between :
 - Collaboration
 - Peer Production
 - Commons Platform/Infrastructures
 - Cooperative model 
 - Self Sustenance
 - Immunity against centralized control
 - Ecologic/Human centric thinking
 - Democracy & Governance in Cooperation
- Technological infrastructure for Cooperatives
- Network effect

### What is wrong with the other current systems/models/infrastructures/platforms ?

- The inability to imagine a different life is capital’s ultimate triumph.
- Teachout recently proposed that one of the pathologies of the current system is that it trains people to be followers.
- It is true that the millions of venture capital put them into a superior position to strike a regulatory sweet spot between the legislative protections that play out in their favor and the calls for corporate responsibilities that do not. 
- The prerogative of VC-backed companies is short-term shareholder profit but when it comes to offering better working conditions, these startups cannot measure up.
- They have serious issues with attrition; the pay rates for workers can (and have been) lowered from one moment to the next, workplace surveillance is constant.
- Command & Control with hierarchy based leadership seems not to be a healthier work place practice.


### What is leadership ?

Understanding the **completeness** of leadership with specificity trained mind set is bit difficult. Take a wider approach of thinking that curiously observe the elements of
organization, interconnections, relationships, entanglements, complexities involved... People usually think that **IDEATING** & **DOING** are two seperate process which 
are linearly connected - because the existing systems train them to think that way. Taking a philosophical roadway, & observing the wider/overall picture will help understand the 
the behavior.

For instance, take a look at this critical [post on leadership](https://medium.com/enspiral-tales/beyond-dreamers-vs-doers-full-circle-leadership-869557da1248#.ilvpd6q3v), by Alanna Irving.
One could see what kind of leadership qualities they naturally have, or can even learn things about leadership which one thinks as lacking.

It has to be informed, that hierarchy is itself not bad at all. But how, when, where, why it has to be used matters. We humans are better at becoming slaves for a structure
and tend to defend it through offensive means. Invisible hierarchy are better killers than visible hierarchy that pollute a distributed leadership (might be bossless),
projecting a new kind of system externall, but rottening internally. To carefully and critically avoid such grave situations, a cooperative needs to learn from the mistake
and also learn from guides, such as this, about [distributed leadership](https://medium.com/enspiral-tales/how-to-grow-distributed-leadership-7f6b25f0361c#.b1ns3vxr5).

### How to bootstrap a bossless org. ?

Well, well, that is awesome to hear and feel.... now suddenly there would be nobody to control me, command me, to order me from unknown source in the hierarchy of
workplace. Suddenly transparency comes to ones hand. OK.

Will it work really ? How come one will function magically without any leadership ? Is it bad to expect one to function on their self leadership ? Again... will it work really ?
This [article](https://medium.com/enspiral-tales/bootstrapping-a-bossless-organisation-in-3-easy-steps-afc653e8f5e6#.yoysyzea2) & [this one](https://medium.com/enspiral-tales/getting-from-a-bossy-to-a-bossless-organisation-in-3-painful-steps-dfebe249f575#.x5f3yby9i) might help..

### What are the Cooperative principles ?

Principles are vital for any organizational practice. They lay the ground platform upon which the organization functions and looks at often for help. The following
principles are provided by [worker-cooperative code](https://github.com/cooperativesuk/workercode/blob/master/SUMMARY.md#summary). These principles are first formally laid out by [**Rochdale society of equitable pioneers in Rochdale**](https://en.wikipedia.org/wiki/Rochdale_Principles), as a set of ideals for the operation of cooperatives.

![Rochdale cooperative principles](res/co-op-principles.png)
