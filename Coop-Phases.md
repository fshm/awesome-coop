## Phases of Cooperative (Tech-worker Cooperative)

Usually, cooperatives do follow **one step at a time** implementation model, which is more feasible and easy to manage, and comprehend. Thus naturally, phases of bootstrapping, operation, maintenance are educated and practiced, by involving all the cooperators collectively. From the book of [Tech-Coopoerative-Howto](https://techworker.coop/sites/default/files/TechCoopHOWTO.pdf), i find the following guidelines might be useful to organize a democratically phased understanding. 

- Find cooperators
- Consolidate a clear vision
- Design your process
- Create a startup plan
- Analyze (who & what) you really are
- Feasibility Check
- Business Plan
- Organizational Plan
- Incorporation & Startup
- Operations

## Where are we now ?

With the above phases as the reference to step by step bootstrapping & sustainable maintenance, we shall compare our present phase with this guideline. This i feel, is necessary to organically
propel ourselves to next stages further and further. It might also help us when we do multiple projects or transitioning to try out a new experiment, protoype and projects...

### Find Cooperators: 

So far, i believe and feel, that the there is a awareness, courage, and progressive mindset change atleast in tech communities & civil societies when compared few years or decade ago. With a 
genuine reason to make a living that does not destroy our passion, that does not eats our health for significant bucks in exchange, i hope more and more people are going to pour in after a
bitter and harsh experience from the profit maximizing system - who have felt injustice to their labour. More and more critical thinkers from other domains would also find it necessary to 
practice a sustainable model that does strike a balance between, justice in labour, passion, health, democratic life style. 

We know that most of the group friends, are good at : (**with various degree of confidence & learning**)

#### Concrete Operational Stuff **[WE ARE HERE RIGHT NOW]**

- Technological
 - web & application development, coding
 - database management
 - network programming
 - security/crypto thing
 - people doing awesome Machine Learning, AI
 - doing self hosting services
 - people who understand Internet, Networking for Community Communication N/W's
 - folks interested in GIS
 - using Open Data to analyze, visualize, data crunch etc..
 - number crunching
 - visual designing, web designing
 - hardware, instrumentation, prototyping
 - documentation, education materials

- Social
 - Target community
 - Market forces
 - Law

#### Abstract & Organizational Stuff

- Socio-Economic-Political theories
- Presentation skills
- Group dynamics & management
- Financial resource management
- Economic resource management
- Infrastructure resource management
- Writing fundamental charter documents (constitution, governance, legal stuff, registration)
- Minutes recording, Consensus, Decision making -- Organization
- Dissent

So far, we have not yet retrospected and introspected what we know in terms of confidence level (stronger in so and so, can do so and so, learning so and so....)
This indicates, that we have to openly share what we are strong at, what we are not strong at (interests in learning), so what knowledge sharing happens at the very moment
when one collaborator knows that the remaining collaborators know the stuffs the he or she might be in need to learn about. Do you see the relations emerging now ????
We are still in this stage....

> We atleast need a tabulation of skill sets, and self rate their confidence in using skills and tools



### Consolidate a Clear Vision

A **collective** needs a clear **vision**. So far, i can able to write **N** sentences that can put the vision and mission statements. But i would like to contribute a set of words
that might help form us a vision collectively.

- genuine making of a living
- democratic, equity, solidarity
- transparent, collaborative & justifiable labour
- profession rhyming with passion - not contradicting
- symbiotic
- voluntary leadership in distributed, dynamic form
- there might not be separate social responsibility, as it is integrated & intertwined with the model
