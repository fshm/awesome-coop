When it comes to starting a cooperative or else joining the existing coops, ideas
and their implementation method plays a vital role.

Usually, addressing local necessities, through products and services would be the
course of ideation, discussion, deliberation, implemenation.

One has to think, that the existing methods & systems of business, modes of production,
that are sustainable - enables our adversary thought process to think critically
to propose alternatives, which i think as the heart beater for the cooperatives.

No matter the idea, implementation mechanism + policy intertwining with it, decides
the business model which in turn influences the mechanism & policy. Every idea
can be implemented in different ways and forms. Similarly if the necessity creates
an idea, and even when it exists already, can be reimplemented in alternative form
adapting the path of unfettered Liberty, Equality, Empathy.

Its a long history of transformation, that we have reached to such an age, where
infrastructures previously owned by a few, are significantly becoming part of 
day to day life of commons. How this got implemented is questionable. Whether the people
participating, volunteering, or exploited routes us to different critical thought
process.

Whenever a emerging infrastructure endangers a available/possible sustainable path,
not just democratic protests are enough, but along with it movement and actions are necessary
that can obsolete the exploiting infrastructure. 

Why ?

- Repairing through hacking is ok ! if the infrastructure is truly open.
- Repairing through hacking is ok ! if the ifnrastructure really runs the modifications.
- Repairing through policy establishing via democracy is ok ! if the infrastructure is not offensive.


## Possible Ideas

Current thought process of majority of people is :

"Why not they create an app for that ?"

It seems, that people would use their **brains** if it is an app !

So, while brainstorming; introspecting; retrospecting ideas,
consider, whether the concept in mind is different from other similar 
implementations out there. Examine, if it is practically implementable
within the projected/estimated duration. Use, design principles (ex: Dieter Rams 10 principles).


### The following list might help one to start think !

- Transit system N/W's  ==> Transit/Transport Information Systems + GIS
 - Creating - enabling static schedule planning, publishing
 - Public authority transit mgmt. system *[TRIUMPHS ARE THERE-NEEDS OPEN DATA POLICY]* {GTFS}
 - Transit mapping(take static data and make a unique transit map - niche of niches) *[NOBODY HERE IS DOING THIS]*
 - Realtime transit aware systems through public participation through anonymization (alternative to transitapp)
 - Geography aware road infrastructure planning *[HAVE TO RESEARCH IN TANDEM WITH INDIAN ROAD TRANSPORT]*
 - Accident auditing/research systems (public authorities) *[BADLY NEEDED]*
 - Public transport Battery Information Systems (Battery fleets in buses needs to be monitoried, to effectively know their behaviour, characteristics... etc) *[BADLY NEEDED]*


- Empowering villages through open data ==> Village Information Systems ==> Village Relationship Systems + GIS
 - Open sharing of open environmental data : local weather, soil type, % of water, produce
 - Water mgmt.
 - Soil mgmt.
 - Crop/Vegetation mgmt.
 - Empowering Anganwadi systems
 - Empowering local self support groups
 - To have a P2P application (web p2p) to exchange the surfplus food made by the locals (ex: cropswap app)


- Scientific Education (Information & Visual Design for Science Communication)
 - Controllable Telescopes (Instrumentation ex: Ultrascope) interfaced with celestia/stellarium for Schools & District Science centres
 - Learning communication systems practically with FOSS, FOSH tools & documents
 - Enabling Scientific - Open data to reach the people through application using Interaction Design


- Grassroots - Textiling N/W
 - Local tailors are badly need of a communication system & design delivery system.


- Grassroots - Communication N/W
 - Communication systems through Community networks (empowering public/communities for eradicating digital divide in communication)


- Grassroots - Food N/W through social auditing
 - P2P (web) based terrace gardening/food produce to eliminate middle man to share food
 - To enable communication between terrace gardening practitioners
 - Community Food inspection N/W through FOSH+FOSS Instruments
 - To have a P2P application (web p2p) to exchange the surfplus food made by the locals (ex: cropswap app)


- Waste management systems through active participation


- Empowering MNREGA systems to accelerate social auditing policy with ease


- Conversational UX systems for service oriented systems


- Cooperative based Open access publishing systems as service to Academy/Research institutions
  - Usage of JATS XML exchange format; Substance; Texture; Stencila & Code for Science projects


- Governance
 - Tools for local governance to people (transparency, open data, followups, tracking, social auditing)
 - How about digitizing democratic constitution of nation, build a learning system that can connect consitutionalized laws with sections and groups in society. 
    It could enable a **feedforward** system which is absent in the present democraries, that lead us to late **demonstrations & protests** which are often supressed
    by police states or state terrorism. Nowadays, the feedforward systems are equipped only by journalism and whistle blowing, which often suffer from subjugation,
    ruthless injustice, disbelief, information pollution, absense of viable evidences...etc. Think about it, having a consitution (we have a living one) digitzed, and 
    a system to relate the fundamental laws of the democracy gauranteed by consitution, to everyones life, will help in sensing, the endangerment of rights, facilities
    which were previously provided. Thus a comparison between pre-law/policy and post-law/policy can be provided, alarmed to the citizens of the country. This would
    effectively equip and arm the civil societies, citizens, activists, constitutional researchers, lawyers, to defend democracy from further polluting...... I dont 
    pollute this document with my thoughts any further... :) we shall discuss about this later.
	
	
- Medical/Biomedical
  - Hospital IT services (ex: Hospital Run, GNUHealth)
  

- Social Support System
 - **Loomio** is the best example of long time expected social information/relationship managment system, that simply helps in decision making, when people rationally thinks and expects to make decisions without any domination, and without any leader other than the demand and ideas itself. This ultimately distributes leadership.
 - We can fork loomio and apply local translation, to help support next social uprise, protest, mobilization.

- Social Transport & Logistics System
 - Enclosure of Transport & Logistics have been a overwhelming situation in present situation. The problem has scaled from local to global unrest. It has repurposed automation to take control over the transport economy. For a local community to sustain, local economy generation is necessary. 
 - Transport systems are very similar to nervous systems that span the whole living system. That is why most democratic actions are always related to blocking roads, and making roads as a collection place for protests and rallies. It has always remained as a place for flow of goods and services. Only by decentrally automating with platforms owned by the workers involved, the problem of scaling shall be met through proper adhoc networking between the geographically distributed local unions, communities & cooperatives.


## Ideation

Usually dreaming - brave dreaming is required which in turn depends upon a concrete infrastrcture that is operational. Both of these help each other as a symbiotic
cycle. I believe you would read and understood about *full circle leadership*. Dreaming is Visionary & Doing is Operational. Both are required for Ideation & Implementation.
In our brains, we can create all ideas possible. Which is feasible for the phase we are in is the pragmatic thing to think. 

**NOTE : 1**: There are many possible ideas we can pitch in... We have to select an idea, which is not only inspiring, but also 

- practically feasible & operational
- ideas that can leverage complementary skills
- enable us to bootstrap properly
- ideas that could fit local social/economic/political necessities
- ideas for localities (where are we going to workout the idea, implementation - to whom, to which set of people ?)
- does the potential target group are ready to accept the outcome/alternative; could be effectively compete the superweed ?
- which class of society does the target group belong to ?
- ideas that support and nurture bootstrapping (most of us are very enthusiastic in providing alternative for existing super weeds)
- is it good to give alternative for a super weed which has entangled its roots deeper into the social sphere ?
- is it wise to go niche ?

**NOTE : 2** In short, we collectively has to draw a **blockdiagrm : mindmap + roadmap of** :

- what is the *product/service* ?
- why do we make it ?
- who are we making it for ?
- which class of society is going to use it ?
- does the product/service would really help their livelihood or profession ?
- are we ready to invest in time & politics to confront the extravaganza competition in front of us ? (possible only with social uprising & mobilization MOMENT)

**NOTE : 3** Remember competition from heavy weight champions will block us, only when they directly rooted with locals & most people are already trained to use it intutively.
But, if the alternative we dream, for a tool/app or service, which does not have much penetration, and when there is merely no usage locally, then we have a window of opportunity,
to even confront the champion, before even entering the locality. Having said that, this triumph requires dedicated continuous work.

## Language & Lexicon

Language influences the way the we think, and thus we need to understand concepts of cooperation, **NOT** in terms of - conventional monopoly/central command & control - influenced terms, but in a more democratic, egalatarian basis - that would encourage creation, courages thinking, progressive implementations. I also believe that we have to build new lexicons that help tweak, progressively the thought process to bring a enthusiastic and +ve change in the collective. For example, later in a cooperative meeting using the following words would empower the doers, dreamers mind.

- liveline ==> instead of deadline
