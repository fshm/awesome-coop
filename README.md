# Coop Resources


## Introduction

- [Cooperative Economics](https://en.wikipedia.org/wiki/Co-operative_economics)
- [Hacking Capitalism](http://wiki.p2pfoundation.net/Hacking_Capitalism)
- [Jim Brown, TED Talk, How to stop poverty: start a worker-owned cooperative](https://www.youtube.com/watch?v=LFyl0zz2yqs)
- [Platform Cooperativism](http://platformcoop.net/), [resources](http://platformcoop.net/resources)
- [International Cooperative Alliance (ICA)](http://ica.coop/en) : [Coop Principles](http://ica.coop/en/what-co-operative), [Blueprint for a cooperative decade](http://ica.staging.adagiocreate.com/sites/default/files/ICA%20Blueprint%20-%20Final%20version%20issued%207%20Feb%2013.pdf)
- [Commons Strategies Group](http://commonsstrategies.org/)
- [Community Land Trust Network](http://cltnetwork.org)
- [Transition Network](https://transitionnetwork.org/)
- [Grass Roots Economic Organizing](http://www.geo.coop/)
- [Coproduction Network](http://coproductionnetwork.com/)
- [Evolution of Cooperatives in India](http://www.pib.nic.in/feature/fe0299/f1202992.html)
- [Vikalp Sangam](http://www.vikalpsangam.org/)
- [NCUI (National Coop Union of India) - Coop movement India](http://www.ncui.coop)
- [Institute for New Economic Thinking](https://www.ineteconomics.org)

> All over the world, individuals, businesses, and cities are involved in the platform cooperativism movement.
There are many ways to get involved: from joining a local group to  starting your own platform co-op. 


## Coops - based Science, Internet, Decentralization with Digital Facet.

- [techworker.coop](https://www.techworker.coop/)
 
> We are worker cooperatives (businesses owned and democratically controlled by our workers)
offering a wide range of media, communications, and computer technology goods and services.

- [Cooperative Technologists : coops.tech](https://www.coops.tech/)
 
> Building a tech industry that's better for its workers and customers through co-operation, democracy and worker ownership.

- [Tech Collective](https://techcollective.com/#welcome)

> TechCollective is a worker-owned cooperative offering enterprise-grade technical support to small businesses & non-profits. 
Our teams in San Francisco & Boston work with hundreds of such clients every year. 
We would love to do the same for your organization! Mac or PC, local or cloud, we've got you covered.

- [Lazooz](http://www.lazooz.org/)

> A Decentralized Transportation Platform owned by the community and utilising vehicles` unused space to create a variety of smart transportation solutions.

- [Fairmondo](https://www.fairmondo.de/global)

> Fairmondo is the project to create a global online-marketplace - owned by its local users. 
This will be realized through a network of autonomous local co-operatives in every country that joins in. 

- [Loomio](https://www.loomio.org/)

> Loomio is open source software, built by a worker-owned cooperative social enterprise. We are based in Aotearoa New Zealand, and are part of the Enspiral Network. 

- [Phone Coop](https://www.thephone.coop/home/)

> At The Phone Co-op, we're all about people. People like you...

- [Protozoa](https://www.protozoa.nz/)

> We make opensource software for humans

- [Tableflip](https://tableflip.io/)

> We build websites, apps, robots, community

- [Faircoop](https://fair.coop/)

> Earth's Cooperative ecosystem for a fair economy

- [Freedomcoop](http://freedomcoop.eu/)

> Where every human being can become a member

- [Open Collective](https://opencollective.com/)

> A new form of association, transparent by design

- [Social Coop](https://opencollective.com/socialcoop#); [Social Coop in Mastodon](https://social.coop/about)

> coop-run corner of the fediverse with a co-operative and transparent approach to operating a social platform

- [CoopCycle](https://coopcycle.org/en/)

> CoopCycle is a delivery platform for worker-owned coops Organize, we provide the technology! 


## Organizing, Managment - Information & Tools

- [Internet of Ownership](https://ioo.coop/), [library](https://ioo.coop/library/), [directory of platforms](https://ioo.coop/directory/)
- [Coop Data](http://coopdata.org/) : CoopData.org uses proven tools and techniques to make important data for the cooperative sector easy to find, use, improve and share. 
- [Vientos](https://vientos.coop/) : Vientos is an open source platform co-op designing ways to facilitate collaboration between social projects, the solidarity economy, and individuals.
- [FairCoin](https://fair-coin.org/en) : Decentralized digital currency organized to provide economic support based on cooperative economic principles unlike capital competition crypto currency.


## Case Studies


[*list of worker cooperatives*](https://en.wikipedia.org/wiki/List_of_worker_cooperatives)

- [Mondragon Cooperative Corporation](https://en.wikipedia.org/wiki/Mondragon_Corporation)
    - [Understanding the Mondragon Worker Cooperative Corporation](https://www.youtube.com/watch?v=8bcNfbGxAdY)
    - [Employee Ownership as Strategy](https://www.youtube.com/watch?v=zijMTOz9eYg)

- [Cooperativa Integral Catalana](https://cooperativa.cat/en/)
    - [Economic Disobedience](https://cooperativa.cat/en/economic-disobedience/)

- [Bank of Commons](https://bankofthecommons.coop/)

- [FairCoop](https://fair.coop/)
   - [FairCoin](https://fair-coin.org/)
    - [FairPay](https://fairtoearth.com/)
    - [FairFund](https://2017.fair.coop/fairfunds/)
    - [FairSaving](https://2017.fair.coop/fairsaving-2/)
    - [FairMarket](https://market.fair.coop/)

## Books & Guides

- [Our to Hack and to Own](res/Schneider_Scholz.epub)
- [Hacking Capitalism](http://downloads.gvsig.org/download/people/vagazzi/Hacking%20Capitalism.pdf)
- [Tyranny of Unstructurelessness](res/tyranny_of_unstructurelessness.pdf)
- [A Technology Freelancer's guide to starting a Worker Cooperative](https://techworker.coop/sites/default/files/TechCoopHOWTO.pdf)
- [Coop handbook - Loomio](https://loomio.coop/)
- [Brian Robertson, Holacracy](res/holacracy.epub)
- [Enric Duran](http://enricduran.cat/en/)

## Papers
- [Ellerman - Role of Capital] (res/Ellerman-On_the_role_of_capital_in_Capitalist_and_Labour_Managed_firms.pdf)
- [Trebor Scholz - Paper about Platform Coop] (http://www.rosalux-nyc.org/wp-content/files_mf/scholz_platformcoop_5.9.2016.pdf)

## Articles

- [Platform Cooperativism by Trebor Scholz](http://www.rosalux-nyc.org/platform-cooperativism-2/)
- [Platform Cooperativism Vs Sharing Economy](https://medium.com/@trebors/platform-cooperativism-vs-the-sharing-economy-2ea737f1b5ad#.z5v3qxhzv)
- [Where does current Peer production goes ? - Politics of Sharing Economy](http://www.publicseminar.org/2014/06/the-politics-of-the-sharing-economy/#.WMWVyeThXeQ)
- [Occupy only is not enough in , Action is needed !](http://www.yesmagazine.org/new-economy/how-a-worker-owned-tech-startup-found-investors-and-kept-its-values-20160426)
- [Centralization & Decentralization tensions in Digital Economy] (http://ictlogy.net/20170413-centralization-vs-decentralization-tensions-in-the-digital-economy/)
